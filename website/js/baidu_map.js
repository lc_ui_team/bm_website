/**
 * Created by Administrator on 2017/6/22.
 */
// 百度地图API功能

var baiduMap=(function () {
    var map = new BMap.Map("map");
    var point = new BMap.Point(113.875364,22.5887);
    map.centerAndZoom(point, 17);
    var marker = new BMap.Marker(point);
    map.addOverlay(marker);
    var infoWindow = new BMap.InfoWindow("地址：深圳市宝安区西乡街道共和工业路明月花都裙楼4楼", {
        width : 200,     // 信息窗口宽度
        height: 80,     // 信息窗口高度
        title : "百码互联" , // 信息窗口标题
        enableMessage:true,//设置允许信息窗发送短息
        message:"戳下面的链接看下地址喔~"});
    marker.setAnimation(BMAP_ANIMATION_BOUNCE);
    marker.addEventListener("click", function(){
        map.openInfoWindow(infoWindow,point); //开启信息窗口
    });
})()
