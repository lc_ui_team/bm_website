

function time() {
    var n = 0;
    var q = 3;
    var r = 0;
    function Tong_MoveDiv() {
        this.Move = function(a, b, T) {
            if (a == "") {
                return
            }
            var o = document.getElementById(a);
            if (!o) {
                return
            }
            evt = b ? b : window.event;
            o.style.position = "absolute";
            o.style.zIndex = 200;
            var c = evt.srcElement ? evt.srcElement : evt.target;
            var w = o.offsetWidth;
            var h = o.offsetHeight;
            var l = o.offsetLeft;
            var t = o.offsetTop;
            var d = document.createElement("DIV");
            document.body.appendChild(d);
            d.style.cssText = "filter:alpha(Opacity=10,style=0);opacity:0.2;width:" + w + "px;height:" + h + "px;top:" + t + "px;left:" + l + "px;position:absolute;background:#000";
            d.setAttribute("id", a + "temp");
            this.Move_OnlyMove(a, evt, T)
        };
        this.Move_OnlyMove = function(d, f, T) {
            var o = document.getElementById(d + "temp");
            if (!o) {
                return
            }
            var g = f ? f : window.event;
            var h = g.clientX - o.offsetLeft;
            var i = g.clientY - o.offsetTop;
            if (!window.captureEvents) {
                o.setCapture()
            } else {
                window.captureEvents(Event.MOUSEMOVE | Event.MOUSEUP)
            }
            document.onmousemove = function(e) {
                if (!o) {
                    return
                }
                e = e ? e : window.event;
                var a = Math.max(document.body.scrollHeight, document.body.clientHeight, document.body.offsetHeight, document.documentElement.scrollHeight, document.documentElement.clientHeight, document.documentElement.offsetHeight);
                var b = Math.max(document.body.scrollWidth, document.body.clientWidth, document.body.offsetWidth, document.documentElement.scrollWidth, document.documentElement.clientWidth, document.documentElement.offsetWidth);
                var c = 0;
                if (document.body.scrollWidth < b) c = document.body.scrollWidth;
                if (document.body.clientWidth < b && c < document.body.clientWidth) c = document.body.clientWidth;
                if (document.body.offsetWidth < b && c < document.body.offsetWidth) c = document.body.offsetWidth;
                if (document.documentElement.scrollWidth < b && c < document.documentElement.scrollWidth) c = document.documentElement.scrollWidth;
                if (document.documentElement.clientWidth < b && c < document.documentElement.clientWidth) c = document.documentElement.clientWidth;
                if (document.documentElement.offsetWidth < b && c < document.documentElement.offsetWidth) c = document.documentElement.offsetWidth;
                if (e.clientX - h <= 0) {
                    o.style.left = 0 + "px"
                } else if (e.clientX - h >= b - o.offsetWidth - 2) {
                    o.style.left = (c - o.offsetWidth - 2) + "px"
                } else {
                    o.style.left = e.clientX - h + "px"
                }
                if (e.clientY - i <= 1) {
                    o.style.top = 1 + "px"
                } else if (e.clientY - i >= a - o.offsetHeight - 30) {
                    o.style.top = (a - o.offsetHeight) + "px"
                } else {
                    o.style.top = e.clientY - i + "px"
                }
            };
            document.onmouseup = function() {
                if (!o) return;
                if (!window.captureEvents) {
                    o.releaseCapture()
                } else {
                    window.releaseEvents(Event.MOUSEMOVE | Event.MOUSEUP)
                }
                var a = document.getElementById(d);
                if (!a) {
                    return
                }
                var b = o.offsetLeft;
                var c = o.offsetTop;
                var l = a.offsetLeft;
                var t = a.offsetTop;
                s.Move_e(d, b, c, l, t, T);
                document.body.removeChild(o);
                o = null
            }
        };
        this.Move_e = function(a, b, c, l, t, T) {
            if (typeof(window["ct" + a]) != "undefined") {
                clearTimeout(window["ct" + a])
            }
            var o = document.getElementById(a);
            if (!o) return;
            var d = st = 8;
            var e = Math.abs(b - l);
            var f = Math.abs(c - t);
            if (e - f > 0) {
                if (f) {
                    d = Math.round(e / f) > 8 ? 8 : Math.round(e / f) * 6
                } else {
                    d = 0
                }
            } else {
                if (e) {
                    st = Math.round(f / e) > 8 ? 8 : Math.round(f / e) * 6
                } else {
                    st = 0
                }
            }
            if (b - l < 0) {
                d *= -1
            }
            if (c - t < 0) {
                st *= -1
            }
            if (Math.abs(l + d - b) < 52 && d) {
                d = d > 0 ? 2 : -2
            }
            if (Math.abs(t + st - c) < 52 && st) {
                st = st > 0 ? 2 : -2
            }
            if (Math.abs(l + d - b) < 16 && d) {
                d = d > 0 ? 1 : -1
            }
            if (Math.abs(t + st - c) < 16 && st) {
                st = st > 0 ? 1 : -1
            }
            if (e == 0 && f == 0) {
                return
            }
            if (T) {
                o.style.left = b + "px";
                o.style.top = c + "px";
                return
            } else {
                if (Math.abs(l + d - b) < 2) {
                    o.style.left = b + "px"
                } else {
                    o.style.left = l + d + "px"
                }
                if (Math.abs(t + st - c) < 2) {
                    o.style.top = c + "px"
                } else {
                    o.style.top = t + st + "px"
                }
                window["ct" + a] = window.setTimeout("Kf54MyMove.Move_e('" + a + "', " + b + " , " + c + ", " + (l + d) + ", " + (t + st) + "," + T + ")", 1)
            }
        }
    };
    var s = new Tong_MoveDiv();
    (function() {
        var h = (document.all) ? true : false;
        var j = /MSIE 6.0/.test(navigator.appVersion) ? true : false;
        if (h && j) {
            try {
                document.execCommand("BackgroundImageCache", false, true)
            } catch (e) {}
        }
        var d = document,
            isStrict = d.compatMode == "CSS1Compat",
            dd = d.documentElement,
            db = d.body,
            m = Math.max,
            tOut = -1,
            lastScrollY = 0,
            na = navigator.userAgent.toLowerCase(),
            div1 = d.getElementById('kfpopupdiv'),
            timerloop, getWH = function() {
                return {
                    h: (isStrict ? dd : db).clientHeight,
                    w: (isStrict ? dd : db).clientWidth
                }
            },
            getS = function() {
                return {
                    t: m(dd.scrollTop, db.scrollTop),
                    l: m(dd.scrollLeft, db.scrollLeft)
                }
            },
            creElm = function(o, t, a) {
                var b = d.createElement(t || 'div');
                for (var p in o) {
                    p == 'style' ? b[p].cssText = o[p] : b[p] = o[p]
                }
                return (a || db).insertBefore(b, (a || db).firstChild)
            },
            loop = function() {
                var t = 10,
                    st = getS().t,
                    c, wh = getWH();
                c = st - div1.offsetTop + Math.floor((wh.h - div1.clientHeight) / 2);
                c != 0 && (div1.style.top = div1.offsetTop + c * 0.1 + 'px', t = 10);
                timerloop = setTimeout(loop, t)
            },
            show = function() {
                var a = getS().t,
                    wh = getWH();
                div1.style.display = "block";
                div1.style.left = ((wh.w - div1.clientWidth) / 2) + "px";
                div1.style.top = ((wh.h - div1.clientHeight) / 2 + a) + "px";
                lastScrollY = a;
                window.setTimeout(kf_moveWithScroll, 1)
            },
            close = function() {
                div1.style.display = 'none';
                if (tOut != -1) {
                    clearTimeout(tOut);
                    tOut = -1
                }
                return true
            },
            talk = function() {
                close()
            },
            kf_lTrim = function(a) {
                while (a.charAt(0) == " ") {
                    a = a.slice(1)
                }
                return a
            },
            kf_rTrim = function(a) {
                var b = a.length;
                while (a.charAt(b - 1) == " ") {
                    a = a.slice(0, b - 1);
                    b--
                }
                return a
            },
            kf_moveWithScroll = function() {
                if (typeof window.pageYOffset != 'undefined') {
                    nowY = window.pageYOffset
                } else if (typeof document.compatMode != 'undefined' && document.compatMode != 'BackCompat') {
                    nowY = document.documentElement.scrollTop
                } else if (typeof document.body != 'undefined') {
                    nowY = document.body.scrollTop
                }
                percent = .1 * (nowY - lastScrollY);
                if (percent > 0) {
                    percent = Math.ceil(percent)
                } else {
                    percent = Math.floor(percent)
                }
                if (div1) {
                    div1.style.top = parseInt(div1.style.top) + percent + "px"
                }
                lastScrollY = lastScrollY + percent;
                tOut = window.setTimeout(kf_moveWithScroll, 1)
            },
            kf_setCookie = function(a, b, c, e, f) {
                var g = a + "=" + escape(b) + ";";
                var d = null;
                if (typeof(c) == "object") {
                    d = c
                } else if (typeof(c) == "number") {
                    d = new Date();
                    d = new Date(d.getFullYear(), d.getMonth(), d.getDate(), d.getHours(), d.getMinutes() + c, d.getSeconds(), d.getMilliseconds())
                }
                if (d) {
                    g += "expires=" + d.toGMTString() + ";"
                }
                if (!e) {
                    g += "path=/;"
                } else if (typeof(e) == "string" && e != "") {
                    g += "path=" + e + ";"
                }
                if (!f && typeof(VS_COOKIEDM) != "undefined") {
                    f = VS_COOKIEDM
                }
                if (typeof(f) == "string" && f != "") {
                    g += "domain=" + f + ";"
                }
                document.cookie = g
            },
            kf_getCookie = function(a) {
                var b = "";
                var c = document.cookie.split("; ");
                var d;
                var i;
                for (i = 0; i < c.length; i++) {
                    d = c[i].split("=");
                    if (d && d.length >= 2 && a == kf_rTrim(kf_lTrim(d[0]))) {
                        b = unescape(d[1])
                    }
                }
                return b
            };
        d.getElementById('kfpopupdiv_close').onclick = close;
        d.getElementById('kfpopupdiv_next').onclick = close;
        if (window.addEventListener) {
            d.getElementById('kfpopupdiv_talk').addEventListener('click', close, false)
        } else if (window.attachEvent) {
            d.getElementById('kfpopupdiv_talk').attachEvent('onclick', close)
        }
        var k = kf_getCookie('kfpopwintimes');
        k = parseInt('0' + k);
        r = parseInt(r);
     /*   if (!k || k < q) {*/
            k = k + 1;
            window.setTimeout(show, n);
            kf_setCookie('kfpopwintimes', k, r ? r : '0', '/', '')
       /* }*/
    })()
}
setTimeout("time()", 3000);