;
(function() {

    'use strict';

    var isMobile = {
        Android: function() {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function() {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function() {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function() {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function() {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
    };
    var mobileMenuOutsideClick = function() {

        $(document).click(function(e) {
            var container = $("#bmhl-offcanvas, .js-bmhl-nav-toggle");
            if (!container.is(e.target) && container.has(e.target).length === 0) {

                if ($('body').hasClass('offcanvas')) {

                    $('body').removeClass('offcanvas');
                    $('.js-bmhl-nav-toggle').removeClass('active');

                }


            }
        });

    };
    var offcanvasMenu = function() {
        var secondaryNav=$(".bmhl-nav");
        $('#page').prepend('<div id="bmhl-offcanvas" />');
        $('#page').prepend('<a href="#" class="js-bmhl-nav-toggle bmhl-nav-toggle bmhl-nav-white"><i></i></a>');
        var clone1 = $('.menu-1 > ul').clone();
        $('#bmhl-offcanvas').append(clone1);
        var clone2 = $('.menu-2 > ul').clone();
        $('#bmhl-offcanvas').append(clone2);

        $('#bmhl-offcanvas .has-dropdown').addClass('offcanvas-has-dropdown');
        $('#bmhl-offcanvas').find('li').removeClass('has-dropdown');

        // Hover dropdown menu on mobile
        $('.offcanvas-has-dropdown').mouseenter(function() {
            var $this = $(this);

            $this.addClass('active').find('ul').slideDown(500, 'easeOutExpo');}).mouseleave(function() {

            var $this = $(this);
            $this.removeClass('active').find('ul').slideUp(500, 'easeOutExpo');
        });

        secondaryNav.find(".menu-1 ul li").on("click",function () {
           $(this).addClass('btn-cta').siblings().removeClass("btn-cta");

        });
        secondaryNav.find(".menu-1 ul li a").on("click",function () {
            event.preventDefault();
            var target= $(this.hash);
            $('body,html').animate({
                    'scrollTop': target.offset().top - secondaryNav.height() + 1
                }, 400
            );

        });






        $(window).resize(function() {

            if ($('body').hasClass('offcanvas')) {

                $('body').removeClass('offcanvas');
                $('.js-bmhl-nav-toggle').removeClass('active');

            }
        });
    };
    var burgerMenu = function() {

        $('body').on('click', '.js-bmhl-nav-toggle', function(event) {
            var $this = $(this);
            if ($('body').hasClass('overflow offcanvas')) {
                $('body').removeClass('overflow offcanvas');
            } else {
                $('body').addClass('overflow offcanvas');
            }
            $this.toggleClass('active');
            event.preventDefault();

        });
    };
    var contentWayPoint = function() {
        var i = 0;
        $('.animate-box').waypoint(function(direction) {

            if (direction === 'down' && !$(this.element).hasClass('animated-fast')) {

                i++;

                $(this.element).addClass('item-animate');
                setTimeout(function() {

                    $('body .animate-box.item-animate').each(function(k) {
                        var el = $(this);
                        setTimeout(function() {
                            var effect = el.data('animate-effect');
                            if (effect === 'fadeIn') {
                                el.addClass('fadeIn animated-fast');
                            } else if (effect === 'fadeInLeft') {
                                el.addClass('fadeInLeft animated-fast');
                            } else if (effect === 'fadeInRight') {
                                el.addClass('fadeInRight animated-fast');
                            } else {
                                el.addClass('fadeInUp animated-fast');
                            }

                            el.removeClass('item-animate');
                        }, k * 200, 'easeInOutExpo');
                    });

                }, 100);

            }

        }, { offset: '85%' });
    };
   /* var dropdown = function() {

        $('.has-dropdown').mouseenter(function() {

            var $this = $(this);
            $this
                .find('.dropdown')
                .css('display', 'block')
                .addClass('animated-fast fadeInUpMenu');

        }).mouseleave(function() {
            var $this = $(this);

            $this
                .find('.dropdown')
                .css('display', 'none')
                .removeClass('animated-fast fadeInUpMenu');
        });

    };*/
    var goToTop = function() {

        $('.js-gotop').on('click', function(event) {

            event.preventDefault();

            $('html, body').animate({
                scrollTop: $('html').offset().top
            }, 500, 'easeInOutExpo');

            return false;
        });

        var menuYloc = $(".qq_callme").offset().top;
        $(window).scroll(function() {
                var offsetTop = menuYloc + $(window).scrollTop() + "px";
                $(".qq_callme").animate({ top: offsetTop }, { duration: 600, queue: false });
            var $win = $(window);
            var nav=$('.bmhl-nav');
            if ($win.scrollTop() > 200) {
                $('.js-top').addClass('active');
                if(isMobile.any()==null){

                    nav.find(".top").fadeOut("slow");
                    nav.css({
                        "position":"fixed",
                        "background":"rgba(0,0,0,.5)"
                    })
                }

            }
            else {
                $('.js-top').removeClass('active');
                if(isMobile.any()==null){
                    nav.find(".top").fadeIn("slow");
                    nav.css({
                        "position":"postion",
                        "background":"none"
                    });
                }

            }

        });

    };
    // Loading page
    var loaderPage = function() {
        $(".bmhl-loader").fadeOut("slow");
    };
    //在校学员
    var counter = function() {
        $('.js-counter').countTo({
            formatter: function(value, options) {
                return value.toFixed(options.decimals);
            },
        });
    };
    var counterWayPoint = function() {
        if ($('#bmhl-counter').length > 0) {
            $('#bmhl-counter').waypoint(function(direction) {
                if (direction === 'down' && !$(this.element).hasClass('animated')) {
                    setTimeout(counter, 400);
                    $(this.element).addClass('animated');
                }
            }, { offset: '90%' });
        }
    };
    var parallax = function() {
        if (!isMobile.any()) {
            $(window).stellar({
                horizontalScrolling: false,
                hideDistantElements: false,
                responsive: true

            });
        }
    };
    var testimonialCarousel = function() {
        $('.owl-carousel-fullwidth').owlCarousel({
            items: 1,
            loop: true,
            margin: 0,
            nav: false,
            dots: true,
            smartSpeed: 800,
            autoHeight: true,
            autoplay:true
        })

    };
   //表单验证
    var FormValidator=function () {
      var strategies = {
          isNonEmpty: function( value, errorMsg ){
              // 不为空
              if ( value === '' ){
                  return errorMsg ;
              }
          },
          minLength: function( value, length, errorMsg ){
              // 限制最小长度
              if ( value.length < length ){
                  return errorMsg;

              }
          },
          isMobile: function( value, errorMsg ){
              if ( !/(^1[3|5|8][0-9]{9}$)/.test( value ) ){
                  return errorMsg;
              }
          }
      };
      var Validator = function(){
          this.cache = []; // 保存校验规则
      };
      Validator.prototype.add = function( dom, rules ){
          var self = this;
          for ( var i = 0, rule; rule = rules[ i++ ]; ){
              (function( rule ){
                  var strategyAry = rule.strategy.split( ':' );
                  var errorMsg = rule.errorMsg;
                  self.cache.push(function(){
                      var strategy = strategyAry.shift();
                      strategyAry.unshift( dom.value );
                      strategyAry.push( errorMsg );
                      return strategies[ strategy ].apply( dom, strategyAry );
                  });
              })(rule)
          }
      };
      Validator.prototype.start = function(){
          for ( var i = 0, validatorFunc; validatorFunc = this.cache[ i++ ]; ){
              var errorMsg = validatorFunc();
              if ( errorMsg ){
                  return errorMsg;
              }
          }
      };
      var registerForm = document.getElementById('registerForm');
      var validataFunc = function(){
          var validator = new Validator();
          validator.add(registerForm.userName, [{
              strategy: 'isNonEmpty',
              errorMsg: '姓名不能为空'
          }]);
          validator.add(registerForm.email, [{
              strategy: 'isNonEmpty',
              errorMsg: '邮箱不能为空'
          }]);
          validator.add(registerForm.phone, [
              {
                  strategy: 'isNonEmpty',
                  errorMsg: '手机不能为空'
              },
              {
                  strategy: 'isMobile',
                  errorMsg: '手机号码格式不正确'
              }]);
          validator.add(registerForm.message,[
              {
                  strategy: 'isNonEmpty',
                  errorMsg: '手机不能为空'
              },
              {
                  strategy: 'minLength:6',
                  errorMsg: '留言长度不能小于6位'
              }

          ])
          var errorMsg = validator.start();
          return errorMsg;
      };
        $("#sub_button").on("click",function () {
            var errorMsg = validataFunc();
            if (errorMsg){
                $("#msg").html(errorMsg);
                return false;
            }else {
                var data= {
                    userName: $("#userName").val(),
                    mail: $("#email").val(),
                    tel: $("#phone").val(),
                    msg: $("#message").val()
                };
                $.ajax({
                    url: "http://tean.wang:3005/mail",
                    type: "get",
                    data: data,
                })
                    .success(function (response) {
                    $("#msg").html("感谢您的提交，我们将尽快回复您");

                })
                    .fail(function () {
                    console.log(arguments);
                }).always(function () {
                    registerForm.reset();
                })
            }

        });
        setTimeout(function () {
            $("#msg").html("请输入想对我们说的话");
        },3000)

  }



    $(function() {
        loaderPage();
        mobileMenuOutsideClick();
        offcanvasMenu();
        burgerMenu();
        contentWayPoint();
     /*   dropdown();*/
        goToTop();
        counterWayPoint();
        counter();
        parallax();
        testimonialCarousel();
         FormValidator();
        setTimeout(function(){
           var qq_list =["1227966973","522890934","519642958","10873483"];
           var kfpopupdivbottom=$(".kfpopupdivbottom");
            for(var ls in qq_list){
                var  $a_src = "tencent://message/?uin="+qq_list[ls]+"&Site=&menu=yes";
                var $img_src="http://wpa.qq.com/pa?p=2:"+qq_list[ls]+":51";
                var qqlist_html=[];
                qqlist_html.push('<a target="_blank" href="'+$a_src+'"><img border="0" alt="咨询课程点这里" src="'+$img_src+'" /></a>');
                $(".qq_callme").append($(qqlist_html.join('')))
            }
            var ku_Modal=['<a class="kfpopupdivbnt_talk" target="_blank" id="kfpopupdiv_talk" href="http://wpa.qq.com/msgrd?v=3&uin='+qq_list[Math.floor(Math.random()*qq_list.length)]+'&Site=百码互联&Menu=yes">',
                ' <img src="images/btn_2.gif" border="0" /></a>',
                ' <a href="javascript:void(0);" target="_parent" class="kfpopupdivbnt_next" id="kfpopupdiv_next">',
                '<img src="images/btn_1.gif" border="0" /></a>'].join("");
            kfpopupdivbottom.append($(ku_Modal));
        }, 1000);

    });


}());